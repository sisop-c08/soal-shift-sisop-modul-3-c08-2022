#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <fcntl.h>

#define _XOPEN_SOURCE 700

int main(int argc, char **argv) {
    int new_socket, server_fd;
    char *file = "hartakarun.zip";
    char buffer[BUFSIZ];
    char protoname[] = "tcp";
    int enable = 1;
    int filefd;
    int i;
    socklen_t client_len;
    ssize_t read_return;
    struct protoent *protoent;
    struct sockaddr_in client_address, server_address;
    unsigned short server_port = 22222u;

    if (argc > 1) {
        file = argv[1];
        if (argc > 2) {
            server_port = strtol(argv[2], NULL, 10);
        }
    }

    protoent = getprotobyname(protoname);
    if (protoent == NULL) {
        perror("getprotobyname");
        exit(EXIT_FAILURE);
    }

    server_fd = socket(
        AF_INET,
        SOCK_STREAM,
        protoent->p_proto
    );

    if (server_fd == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) < 0) {
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(EXIT_FAILURE);
    }

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(server_port);
    
    if (bind(
            server_fd,
            (struct sockaddr*)&server_address,
            sizeof(server_address)
        ) == -1
    ) {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 5) == -1) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    fprintf(stderr, "Listening on port %d\n", server_port);

    while (1) {
        client_len = sizeof(client_address);

        puts("waiting for client...");

        new_socket = accept(
            server_fd,
            (struct sockaddr*)&client_address,
            &client_len
        );

        filefd = open(file,
                O_WRONLY | O_CREAT | O_TRUNC,
                S_IRUSR | S_IWUSR);

        if (filefd == -1) {
            perror("open");
            exit(EXIT_FAILURE);
        }
        
        do {
            read_return = read(new_socket, buffer, BUFSIZ);
            if (read_return == -1) {
                perror("read");
                exit(EXIT_FAILURE);
            }
            if (write(filefd, buffer, read_return) == -1) {
                perror("write");
                exit(EXIT_FAILURE);
            }
        } while (read_return > 0);

        close(filefd);
        close(new_socket);
    }

    return EXIT_SUCCESS;
}