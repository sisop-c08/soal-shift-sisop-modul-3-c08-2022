#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <pthread.h>
#include <unistd.h>
#include <ctype.h>
#include <stdbool.h>

int count = 0;
int unknown = 0;
int unused_dir = 0;
char all_file[300][500];
char current_path[300];
char unused_dirs[300][500];

void make_dir(void *name) {
    char *file_name = (char *) name;
    char path[100];

    strcpy(path, current_path);
    strcat(path, "/");
    strcat(path, file_name);
    
    int dir = mkdir(path, 0755);
    if (dir == -1) {
        // fprintf(stderr, "error: Cannot create dir # %s\n", path);
    }
}

void convert_tolower(char *path) {
    int i = 0;
    for (i; path[i]; i++)
        path[i] = tolower(path[i]);
}

void *move_file(void *path) {
    char path_to_file[300], file_name[300], file_ext[30], temp[300];
    char *breaks;

    strcpy(temp, path);
    breaks = strtok(temp, "/");

    while (breaks != NULL) {
        if (breaks != NULL) strcpy(file_name, breaks);
        breaks = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    strcpy(path_to_file, path);

    breaks = strtok(temp, ".");
    breaks = strtok(NULL, "");
    
    if (file_name[0] == '.') {
        strcpy(file_ext, "Hidden");
    } else if (breaks != NULL) {
        strcpy(file_ext, breaks);
    } else {
        strcpy(file_ext, "Unknown");
    }

    convert_tolower(file_ext);

    strcpy(temp, current_path);
    strcat(temp, "/");
    strcat(temp, file_ext);
    strcat(temp, "/");
    strcat(temp, file_name);

    make_dir(file_ext);
    rename(path_to_file, temp);
}

void categorize_file() {
    int err;
    pthread_t thread[1000];

    for (int i = 0; i < count; i++) {

        char *path = (char*) all_file[i];
        err = pthread_create(&thread[i], NULL, &move_file, (void *) path);

        if (err != 0)
            printf("\n can't create thread : [%s]\n", strerror(err));
        else
            printf("\n create thread success %d\n", i);
    }
    
    for (int i = 0; i < count; i++) {
        if (pthread_join(thread[i], NULL)) {
            fprintf(stderr, "error: Cannot join thread # %d\n", i);
        }
    }
}

bool is_file_unknown(void *path) {
    char path_to_file[300], file_name[300], file_ext[30], temp[300];
    char *breaks;

    strcpy(temp, path);
    breaks = strtok(temp, "/");

    while (breaks != NULL) {
        if (breaks != NULL){
            strcpy(file_name, breaks);
        }
        
        breaks = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    strcpy(path_to_file, path);

    breaks = strtok(temp, ".");
    breaks = strtok(NULL, "");
    
    if (file_name[0] == '.') {
        return true;
    } else if (breaks != NULL) {
        return true;
    } else {
        return false;
    }
}

void list_file(char *path){
    char file_path[300], temp[300];
    bool is_unknown = false;

    struct dirent *dp;
    DIR *dir = opendir(path);
    
    if (!dir) return;

    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
            strcpy(file_path, path);
            strcat(file_path, "/");
            strcat(file_path, dp->d_name);

			if(dp->d_type == DT_DIR){
                strcpy(unused_dirs[unused_dir], file_path);
                unused_dir++;

                list_file(file_path);
            }else if(dp->d_type == DT_REG){
                is_unknown = is_file_unknown(file_path);

                if (is_unknown) {
                    strcpy(all_file[count], file_path);
                } else {
                    strcpy(temp, all_file[unknown]);
                    strcpy(all_file[unknown], file_path);
                    strcpy(all_file[count], temp);
                    unknown++;
                }

                count++;
            }
        }
    }

    closedir(dir);
}

void remove_unused_dir(){
    for(int i=unused_dir-1; i >= 0; i--){
        rmdir(unused_dirs[i]);
    }
}

int main(int argc, char *argv[]) {
	char path[] = "/home/dhika/shift3/hartakarun";
	chdir(path);

	getcwd(current_path, sizeof(current_path));

    list_file(path);
    categorize_file();
    remove_unused_dir();

	return 0;
}