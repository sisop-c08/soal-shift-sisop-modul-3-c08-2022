#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define PORT 8080
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct login                           // before the first use of `l`.
{
    char username[10];
    char password[10];
};



void login (void);
void registration (void);

void login (void){
    char username[30],password[20];
    FILE *log;

    log = fopen("login.txt","r");
    if (log == NULL){
        fputs("Error at opening File!", stderr);
        exit(1);
    }

    struct login l;

    printf("\nPlease Enter your login credentials below\n\n");
    printf("Username:  ");
    fgets(username, 30, stdin);
    printf("\nPassword: ");
    printf("\n");
    fgets(password, 20, stdin);

    while(fread(&l,sizeof(l),1,log)){
        if(strcmp(username,l.username)==1 || strcmp(password,l.password)==1){   
                printf("\nSuccessful Login\n");
        }
        else {
                printf("\nIncorrect Login Details\nPlease enter the correct credentials\n");
        }
    }

    fclose(log);

    return;
}




void registration(void){
    FILE *log;

    log=fopen("login.txt","w");
    if (log == NULL){
        fputs("Error at opening File!", stderr);
        exit(1);
    }
    struct login l;
    memset(l.username,0,sizeof(l.username));
    memset(l.password,0,sizeof(l.password));

    printf("\nEnter Username:\n");
    scanf("%s",l.username);
    printf("\nEnter Password:\n");
    scanf("%s",l.password);


    fwrite(&l.username,sizeof(l.username),1,log);
    fwrite(&l.password,sizeof(l.password),1,log);
    fclose(log);

    printf("\nConfirming details...\n...\nWelcome, %s!\n\n",l.username);
    printf("\nRegistration Successful!\n");
    printf("Press any key to continue...");
        getchar();
    login();
}

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }


    int option;

    printf("Press '1' to Register\nPress '2' to Login\n\n");
    scanf("%d",&option);

    getchar();           // catching newline.

    if(option == 1)
        {
            registration();
        }

    else if(option == 2)
        {
            login();
        }
    // send(sock , hello , strlen(hello) , 0 );
    // printf("Hello message sent\n");
    // valread = read( sock , buffer, 1024);
    // printf("%s\n",buffer );
    return 0;
}