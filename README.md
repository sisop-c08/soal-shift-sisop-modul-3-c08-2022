# Laporan Praktikum Modul 3
# Identitas Kelompok
### Kelompok C08
 1.  __Doanda Dresta Rahma	5025201049__
 2.  __Putu Andhika Pratama	5025201132__
 3.  __Muhammad Raihan Athallah	5025201206__

# Daftar Isi
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)


# Soal 1
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

a.	Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

b.	Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

c.	Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

d.	Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

e.	Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
## 1. Download dan Unzip
Didefinisikan string yang diperlukan, seperti link download dan lain-lain
```c
int main(void)
{
    char *musicLink = "https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1";
    char *quoteLink = "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt";
    char *musicZip = "music.zip";
    char *quoteZip = "quote.zip";
    char *hasilZip = "hasil.zip";
    char *musicDir = "music/";
    char *quoteDir = "quote/";
    char *hasilDir = "hasil/";
    char *musicTxt = "music.txt";
    char *quoteTxt = "quote.txt";
    char *password = "mihinomestdoanda";
    char *noTxt = "no.txt";
```
Karena harus mendownload files dan unzip secara bersamaan, bisa menggunakan thread.
Untuk membuat thread dengan pthread_create, diperlukan thread (pthread_t), argumennya (opsional), dan functionnya.
Proses download harus selesai sebelum file di unzip, maka itu thread untuk unzip hanya dibuat ketika thread download selesai, hal ini bisa dilakukan dengan menjoinkan thread download sebelum membuat thread unzip.
```c
    pthread_t download_t[2];
    char *dlArgs1[] = {"wget", "--no-check-certificate", musicLink, "-O", musicZip, NULL};
    char *dlArgs2[] = {"wget", "--no-check-certificate", quoteLink, "-O", quoteZip, NULL};
    pthread_create(&(download_t[0]),NULL,&download, (void*) dlArgs1);  
    pthread_create(&(download_t[1]),NULL,&download, (void*) dlArgs2);  
    pthread_join(download_t[0], NULL);
    pthread_join(download_t[1], NULL);

    pthread_t unzip_t[2];
    char *unzipArgs1[] = {"unzip", "-d", musicDir, musicZip, NULL};
    char *unzipArgs2[] = {"unzip", "-d", quoteDir, quoteZip, NULL};
    pthread_create(&(unzip_t[0]),NULL,&unzip, (void*) unzipArgs1);  
    pthread_create(&(unzip_t[1]),NULL,&unzip, (void*) unzipArgs2);  
    pthread_join(unzip_t[0], NULL);
    pthread_join(unzip_t[1], NULL);
```
Fungsi download mengambil dan men-typecast argumen array of strings yaitu argumen execv untuk melakukan command wget untuk mendownload.
```c
void *download( void *arg )
{
    char **argv = (char **) arg;

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		execv("/bin/wget", argv);
	} else {
		while(wait(&status) > 0);
	}    
}
```
Fungsi unzip seperti download, dengan execv untuk melakukan command unzip
```c
void *unzip( void *arg )
{
    char **argv = (char **) arg;

	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		execv("/bin/unzip", argv);
	} else {
		while(wait(&status) > 0);
        char *zip = argv[3];
        removeFile(zip);
	}    
}
```
## 2. Decode
Decoding file-file pada kedua folder bersamaan, dilakukan dengan thread
```c
    pthread_t decode_t[2];
    char *decodeArgs1[] = {musicDir, musicTxt};
    char *decodeArgs2[] = {quoteDir, quoteTxt};
    pthread_create(&(decode_t[0]),NULL,&decode, (void*) decodeArgs1); 
    pthread_create(&(decode_t[1]),NULL,&decode, (void*) decodeArgs2); 
    pthread_join(decode_t[0], NULL);
    pthread_join(decode_t[1], NULL);    
```
Dalam fungsi decode, menerima argumen folder dimana file txt berada dan file text yang dihasilkan.
Membuat dan membuka file txt output, mengakses direktori dan iterate tiap file txt kecuali output dan ./.. , Mengambil string base64, melakukan decode, lalu menuliskan hasil ke file txt output.
```c
void *decode( void *arg ) 
{
    char **argv = (char **) arg;
    char *dir = argv[0];
    char *txt = argv[1];

    char writePath[100];
    sprintf(writePath, "%s%s", dir, txt);

	FILE *fpw = fopen(writePath, "w");
	if (fpw == NULL)
		printf("error opening file\n");
	DIR *dp;
	struct dirent *dirp;
    dp = opendir(dir);
    while ((dirp = readdir(dp)) != NULL) {
        if (strcmp(dirp->d_name, ".") != 0 && 
            strcmp(dirp->d_name, "..") != 0 && 
            strcmp(dirp->d_name, txt) != 0) 
        {
            char readPath[256];
            sprintf(readPath, "%s%s", dir, dirp->d_name);

            FILE *fpr = fopen(readPath, "r");
            if (fpr == NULL)
                printf("error opening file\n");

            char encoded[256];
            fgets(encoded, sizeof(encoded), fpr);
            fclose(fpr);

            long decode_size = strlen(encoded);
            char *decoded = base64_decode(encoded, decode_size, &decode_size);
	    	fprintf(fpw, "%s\n", decoded);
        }
    }
    fclose(fpw);
    closedir(dp);
}
```
Proses decode melibatkan tabel untuk encoding. Dari itu dibuat tabel decoding. Algoritma decoding melibatkan decoding table, 32-bit integer, dan manipulasi bitwise.
```c
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;

void build_decoding_table() 
{
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) 
{
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }
 
    return decoded_data;
}
```
## 3. Pindahkan file
File hasil.txt dan quote.txt dipindah ke folder hasil. folder hasil dibuat, lalu file txt dipindah menggunakan move, terakhir folder music dan quote dihapus. Fungsi makeDir, move, dan removeDir menggunakan spawning proses dengan execv.
```c
    makeDir(hasilDir);
    move(musicDir, musicTxt, hasilDir);
    move(quoteDir, quoteTxt, hasilDir);

    removeDir(musicDir);
    removeDir(quoteDir);

void makeDir(char *path) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/bin/mkdir", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void move(char *dir, char *file, char *dest) 
{
	char source[100];
    sprintf(source, "%s%s", dir, file);
	
	pid_t child_id;	
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mv", source, dest, NULL};
		execv("/bin/mv", argv);
	} else {
		while(wait(&status) > 0);
	}
}

void removeDir(char *dir) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"rm", "-r", dir, NULL};
		execv("/bin/rm", argv);
	} else {
		while(wait(&status) > 0);
	}
}
```
## 4. Zip dengan password
Zip dengan password menggunakan fungsi zipPass, lalu hapus folder hasil
```c
    zipPass(password, hasilZip, hasilDir);
    removeDir(hasilDir);
```
Fungsi zipPass melakukan process spawning untuk melakukan execv, meng-execute command bash zip
```c
void zipPass(char *password, char *zip, char *file) 
{
	pid_t child_id;
	int status;
	child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"zip", "--password", password, "-r", zip, file, NULL};
		execv("/bin/zip", argv);
	} else {
		while(wait(&status) > 0);
        
	}
}
```
## 5. Unzip, masukkan txt, zip
Unzip dan membuat txt dilakukan bersamaan dengan thread. menggunakan fungsi unzip dan makeTxt. no.txt dibuat di folder awal, setelah unzip no.txt dimasukkan ke folder hasil. Lalu, folder hasil di zip dengan password yang sama.
```c
    pthread_t unzipPass_t;
    pthread_t makeTxt_t;
    char *unzipPassArgs[] = {"unzip", "-P", password, hasilZip, NULL}; 
    char *makeTxtArgs[] = {noTxt, "No"};
    pthread_create(&(unzipPass_t),NULL,&unzip, (void*) unzipPassArgs);  
    pthread_create(&(makeTxt_t),NULL,&makeTxt, (void*) makeTxtArgs);  
    pthread_join(unzipPass_t, NULL);
    pthread_join(makeTxt_t, NULL);

    move("", noTxt, hasilDir);
    zipPass(password, hasilZip, hasilDir);
    removeDir(hasilDir);

    exit(EXIT_SUCCESS);
}
```
Fungsi makeTxt membuka file txt dengan nama dari string pertama dari argumen array of strings, lalu memasukkan string kedua ke file txt tersebut
```c
void *makeTxt( void *arg ) 
{
    char **argv = (char **) arg;

    FILE *fp = fopen(argv[0],"w");
    if (fp == NULL)
        printf("error opening file\n");
    fprintf(fp, "%s", argv[1]);
    fclose(fp);
}
```
### Hasil Run
![zip file](img/zipfile1.jpg)
![extrak file](img/extrakfile1.jpg)
![run program](img/musik.jpg)
![run program](img/no.jpg)
![run program](img/quote.jpg)

# Soal 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:


Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
Username unique (tidak boleh ada user yang memiliki username yang sama)
Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
Format users.txt:

## CLIENT
```c
struct login                           // before the first use of `l`.
{
    char username[10];
    char password[10];
};



void login (void);
void registration (void);

void login (void){
    char username[30],password[20];
    FILE *log;

    log = fopen("login.txt","r");
    if (log == NULL){
        fputs("Error at opening File!", stderr);
        exit(1);
    }

    struct login l;

    printf("\nPlease Enter your login credentials below\n\n");
    printf("Username:  ");
    fgets(username, 30, stdin);
    printf("\nPassword: ");
    printf("\n");
    fgets(password, 20, stdin);

    while(fread(&l,sizeof(l),1,log)){
        if(strcmp(username,l.username)==1 || strcmp(password,l.password)==1){   
                printf("\nSuccessful Login\n");
        }
        else {
                printf("\nIncorrect Login Details\nPlease enter the correct credentials\n");
        }
    }

    fclose(log);

    return;
}




void registration(void){
    FILE *log;

    log=fopen("login.txt","w");
    if (log == NULL){
        fputs("Error at opening File!", stderr);
        exit(1);
    }
    struct login l;
    memset(l.username,0,sizeof(l.username));
    memset(l.password,0,sizeof(l.password));

    printf("\nEnter Username:\n");
    scanf("%s",l.username);
    printf("\nEnter Password:\n");
    scanf("%s",l.password);


    fwrite(&l.username,sizeof(l.username),1,log);
    fwrite(&l.password,sizeof(l.password),1,log);
    fclose(log);

    printf("\nConfirming details...\n...\nWelcome, %s!\n\n",l.username);
    printf("\nRegistration Successful!\n");
    printf("Press any key to continue...");
        getchar();
    login();
}

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }


    int option;

    printf("Press '1' to Register\nPress '2' to Login\n\n");
    scanf("%d",&option);

    getchar();           // catching newline.

    if(option == 1)
        {
            registration();
        }

    else if(option == 2)
        {
            login();
        }
    return 0;
}

```

## SERVER
```c
int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    return 0;
}

}

```
### Hasil Run
![run program](img/register.jpg)
# Soal 3
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang,
Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan
jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan,
Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
Contoh jika program pengkategorian dijalankan

a. Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan
ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan
berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada
file yang tertinggal, program harus mengkategorikan seluruh file pada working
directory secara rekursif

b. Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki
ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder
“Hidden”.

c. Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan
dioperasikan oleh 1 thread.

d. Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program
client-server. Saat program client dijalankan, maka folder
/home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama
“hartakarun.zip” ke working directory dari program client.

e. Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan
command berikut ke server

Note : karena belum dapat melakukan download, unzip, dan zip tanpa system, fork dan exec maka untuk sementara dilakukan secara manual

## 1. Listing File
dengan fungsi `list_file` melakukan listing secara rekursif pada semua file di folder hartakarun, sebelum dicatat setiap file akan dicek ekstensinya dengan fungsi `is_file_unknown`, setelah itu seluruh path file akan dicatat pada `char all_file[300][500]`;
```c
void list_file(char *path){
    char file_path[300], temp[300];
    bool is_unknown = false;

    struct dirent *dp;
    DIR *dir = opendir(path);
    
    if (!dir) return;

    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
            strcpy(file_path, path);
            strcat(file_path, "/");
            strcat(file_path, dp->d_name);

			if(dp->d_type == DT_DIR){
                strcpy(unused_dirs[unused_dir], file_path);
                unused_dir++;

                list_file(file_path);
            }else if(dp->d_type == DT_REG){
                is_unknown = is_file_unknown(file_path);

                if (is_unknown) {
                    strcpy(all_file[count], file_path);
                } else {
                    strcpy(temp, all_file[unknown]);
                    strcpy(all_file[unknown], file_path);
                    strcpy(all_file[count], temp);
                    unknown++;
                }

                count++;
            }
        }
    }

    closedir(dir);
}
```

Pada fungsi `is_file_unknown` jika file yang dicek tidak memiliki ekstensi maka akan mengembalikan nilai `false`
```c
bool is_file_unknown(void *path) {
    char path_to_file[300], file_name[300], file_ext[30], temp[300];
    char *breaks;

    strcpy(temp, path);
    breaks = strtok(temp, "/");

    while (breaks != NULL) {
        if (breaks != NULL){
            strcpy(file_name, breaks);
        }
        
        breaks = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    strcpy(path_to_file, path);

    breaks = strtok(temp, ".");
    breaks = strtok(NULL, "");
    
    if (file_name[0] == '.') {
        return true;
    } else if (breaks != NULL) {
        return true;
    } else {
        return false;
    }
}
```

## 2. Kategorisasi File
Pada fungsi `categorize_file` menggunakan multithread untuk melakukan kategorisasi file
```c
void categorize_file() {
    int err;
    pthread_t thread[1000];

    for (int i = 0; i < count; i++) {

        char *path = (char*) all_file[i];
        err = pthread_create(&thread[i], NULL, &move_file, (void *) path);

        if (err != 0)
            printf("\n can't create thread : [%s]\n", strerror(err));
        else
            printf("\n create thread success %d\n", i);
    }
    
    for (int i = 0; i < count; i++) {
        if (pthread_join(thread[i], NULL)) {
            fprintf(stderr, "error: Cannot join thread # %d\n", i);
        }
    }
}
```

## 3. Memindahkan File ke Folder Baru
Pada fungsi `move_file` tiap file akan dipindahkan kefolder yang sesuai dengan ekstensinya
```c
void *move_file(void *path) {
    char path_to_file[300], file_name[300], file_ext[30], temp[300];
    char *breaks;

    strcpy(temp, path);
    breaks = strtok(temp, "/");

    while (breaks != NULL) {
        if (breaks != NULL) strcpy(file_name, breaks);
        breaks = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    strcpy(path_to_file, path);

    breaks = strtok(temp, ".");
    breaks = strtok(NULL, "");
    
    if (file_name[0] == '.') {
        strcpy(file_ext, "Hidden");
    } else if (breaks != NULL) {
        strcpy(file_ext, breaks);
    } else {
        strcpy(file_ext, "Unknown");
    }

    convert_tolower(file_ext);

    strcpy(temp, current_path);
    strcat(temp, "/");
    strcat(temp, file_ext);
    strcat(temp, "/");
    strcat(temp, file_name);

    make_dir(file_ext);
    rename(path_to_file, temp);
}
```

## 4. Menghapus Folder yang Tidak Terpakai
Pada folder hartakarun pertama kali terdapat folder yang sudah tidak digunakan, folder tersebut akan dihapus dengan fungsi `remove_unused_dir`
```c
void remove_unused_dir(){
    for(int i=unused_dir-1; i >= 0; i--){
        rmdir(unused_dirs[i]);
    }
}
```

## 5. Server.c untuk menerima zip file
Diawali dengan membuka file `hartakarun.zip` jika belum ada akan dibuat filenya
```c
 filefd = open(file,
            O_WRONLY | O_CREAT | O_TRUNC,
            S_IRUSR | S_IWUSR);

```

Kemudian dengan menggunakan looping akan membaca input dari client kemudian akan dimasukkan ke dalam file `hartakarun.zip`
```c
do {
            read_return = read(new_socket, buffer, BUFSIZ);
            
            if (read_return == -1) {
                perror("read");
                exit(EXIT_FAILURE);
            }

            if (write(filefd, buffer, read_return) == -1) {
                perror("write");
                exit(EXIT_FAILURE);
            }
        } while (read_return > 0);
```

## 6. Client.c untuk mengirim zip file
Diawali dengan membuka file `hartakarun.zip`
```c
filefd = open(file_input, O_RDONLY);
```

Kemudian dengan looping akan membaca file `hartakarun.zip` lalu mengirimkannya ke server
```c
while (1) {
        read_return = read(filefd, buffer, BUFSIZ);
        if (read_return == 0)
            break;
    
        if (read_return == -1) {
            perror("read");
            exit(EXIT_FAILURE);
        }

        if (write(sock, buffer, read_return) == -1) {
            perror("write");
            exit(EXIT_FAILURE);
        }
    }
```
### Hasil Run
![zip file](img/hasilrun22.jpg)
![extrak file](img/hartakarun2.jpg)
![run program](img/hasilrun2.jpg)
![run program](img/22.jpg)
![run program](img/hasilrunserver.jpg)
